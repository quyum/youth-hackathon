<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ env('APP_NAME') }}</title>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/0d8e5e6c23.js" crossorigin="anonymous"></script>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
        <script src="{{ mix('/js/app.js') }}" defer></script>
    </head>
    <body class="font-roboto min-h-screen">
        @inertia
        <script src="https://kit.fontawesome.com/0d8e5e6c23.js"> </script>
    </body>
</html>
