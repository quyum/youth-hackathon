<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Auth\LogoutController;
use App\Http\Controllers\Admin\Auth\RegisterController;
use App\Http\Controllers\Admin\Course\CourseController;
use App\Http\Controllers\Admin\Auth\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\School\SchoolController;
use App\Http\Controllers\User\Course\CategoryController;
use App\Http\Controllers\User\Course\CourseController as UserCourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return inertia('Static/Home');
});
Route::prefix('course')->group(function() {
    Route::get('', [UserCourseController::class, 'index']);
    Route::get('category/{category}', [CategoryController::class, 'index']);
});
Route::prefix('admin')->group(function(){
    Route::middleware('auth.admin')->group(function(){
        Route::prefix('course')->group(function() {
            Route::get('', [CourseController::class, 'index']);
            Route::post('', [CourseController::class, 'create']);
            Route::put('{course}', [CourseController::class, 'update']);
            Route::delete('{course}', [CourseController::class, 'delete']);
            Route::get('edit/{course}', [CourseController::class, 'edit']);
        });
        Route::get('dashboard', [DashboardController::class, 'index']);
        Route::post('logout', [LogoutController::class, 'create']);
    });
    Route::get('login', [LoginController::class, 'index']);
    Route::post('login', [LoginController::class, 'create']);
    Route::get('register', [RegisterController::class, 'index']);
    Route::post('register', [RegisterController::class, 'create']);
});