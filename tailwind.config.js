module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#6246EA',
        secondary: '#2B2C34',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
