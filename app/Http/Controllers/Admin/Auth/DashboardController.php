<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use App\Models\Review\Review;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        return inertia('Admin/Auth/Dashboard', [
            'courses' => Course::count(),
            'reviews' => Review::count(),
        ]);
    }
}
