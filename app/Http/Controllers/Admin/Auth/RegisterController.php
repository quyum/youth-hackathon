<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Enums\UserType;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index() {
        return inertia('Admin/Auth/Register');
    }

    public function create(Request $request) {
        $data = $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'type' => UserType::Administrator,
            'password' => Hash::make($data['password'])
        ]);
        Auth::login($user);
        return redirect('/admin/dashboard');
    }
}
