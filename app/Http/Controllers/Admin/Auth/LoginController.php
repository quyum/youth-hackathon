<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return inertia('Admin/Auth/Login');
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if (Auth::attempt($data, $request->remember)) {
            $request->session()->regenerate();
            return redirect()->intended('admin/dashboard');
        }
        return redirect()->back()->withErrors([
            'error' => 'Incorrect username or password.'
        ]);
    }
}
