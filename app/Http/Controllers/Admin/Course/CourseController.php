<?php

namespace App\Http\Controllers\Admin\Course;

use App\Enums\SupportedCourse;
use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Response;

class CourseController extends Controller
{
    public function index(): Response
    {
        return inertia('Admin/Course/Index', [
            'supportedCourse' => $this->getSupportedCourse(),
            'courses' => Course::latest()->paginate(10),
        ]);
    }

    public function create(Request $request): RedirectResponse
    {
        $data = $this->validateData($request);
        Course::create($data);
        return redirect()->back()->withErrors([
            'success' => 'Course added successfully!'
        ]);
    }

    public function delete(Course $course): RedirectResponse
    {
        $course->delete();
        return redirect()->back()->withErrors([
            'success' => 'Course deleted successfully!'
        ]);
    }

    public function edit(Course $course): Response
    {
        return inertia('Admin/Course/Edit', [
            'course' => $course,
            'supportedCourse' => $this->getSupportedCourse(),
        ]);
    }

    public function update(Request $request, Course $course): RedirectResponse
    {
        $data = $this->validateData($request, $course->id);
        $course->update($data);
        return redirect()->back()->withErrors([
            'success' => 'School updated successfully!'
        ]);
    }

    public function validateData(Request $request, int $id = null): array
    {
        return $request->validate([
            'title' => ['required', 'string'],
            'platform' => ['required', 'string'],
            'certificate' => ['required', 'boolean'],
            'description' => ['string', 'nullable'],
            'slug' => ['required', Rule::unique('courses')->ignore($id), 'alpha_num'],
            'category' => Rule::in(SupportedCourse::Categories),
            'url' => ['required', 'url'],
        ]);
    }

    public function getSupportedCourse(): array
    {
        return [
            'categories' => SupportedCourse::Categories
        ];
    }
}
