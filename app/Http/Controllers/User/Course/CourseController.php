<?php

namespace App\Http\Controllers\User\Course;

use App\Enums\SupportedCourse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index() 
    {
        return inertia('User/Course/Index', [
            'categories' => SupportedCourse::Categories,
        ]);
    }
}
