<?php

namespace App\Http\Controllers\User\Course;

use App\Enums\SupportedCourse;
use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($category) 
    {
        if (!in_array($category, SupportedCourse::Categories)) {
            return redirect('/course');
        }
        $courses = Course::where('category', '=', $category)
            ->latest()
            ->paginate(10);
        return inertia('User/Course/Category', [
            'courses' => $courses,
            'category' => $category,
        ]);
    }
}
