<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SupportedCourse extends Enum
{
    const Categories = ['Computer Science', 'Medicine', 'Law', 'Business', 'Management', 'Engineering', 'Hospitality'];
}
